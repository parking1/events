package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@ApplicationScoped
public class OwnerIsResidentUpdatedEvent extends EventBase<OwnerIsResidentUpdatedEvent> {
    public String ownerId;
    public String isResident;

    @Incoming("owner-is-resident-updated-incoming")
    public void notify(String e){
        this.notifySubscribers(e, this.getClass());
    }

    public static OwnerIsResidentUpdatedEvent with(ObjectId ownerId, String isResident) {
        OwnerIsResidentUpdatedEvent e = new OwnerIsResidentUpdatedEvent();
        e.channelName = "owner-is-resident-updated";
        e.ownerId = getObjectIdAsString(ownerId);
        e.isResident = isResident;
        return e;
    }

    @Outgoing("owner-is-resident-updated")
    public Flowable<String> createStreamEmitter() {
        return this.createStream("owner-is-resident-updated");
   }
}