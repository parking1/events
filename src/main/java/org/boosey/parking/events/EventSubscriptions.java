package org.boosey.parking.events;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EventSubscriptions{

    private static final EventSubscriptions instance = new EventSubscriptions();

    @SuppressWarnings("rawtypes")
    private Map<Class, Map<UUID, Object>> classToSubscriberMap = new HashMap<Class, Map<UUID, Object>>();

    private EventSubscriptions() {}

    public static <T> void subscribe(UUID uuid, Consumer<T> consumer, Class<T> clazz) {
        log.info("Subscribing {}", uuid);
        synchronized(instance){

            Optional.ofNullable(instance.classToSubscriberMap.get(clazz))
                .orElse(instance.classToSubscriberMap.put(clazz, new HashMap<UUID, Object>()));

            Map<UUID, Object> subs = instance.classToSubscriberMap.get(clazz);

            log.info("instance classMap: {}", instance.classToSubscriberMap);
            log.info("clazz: {} subs: {} consumer: {}", clazz, subs, consumer);
            subs.put(uuid, consumer);
            log.info("SUCCESSFUL Subscribing uuid: {} subs: {} consumer from map: {}", uuid, subs, subs.get(uuid));
        }
    }

    public static <T> void unsubscribe(UUID uuid, Class<T> clazz) {
        Map<UUID, Object> subs = getSubscriptionsFor(clazz);
        subs.remove(uuid);
    }

    public static <T> Map<UUID, Object> getSubscriptionsFor(Class<T> clazz){
        return instance.classToSubscriberMap.get(clazz);
    }

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
}
