package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@ApplicationScoped
public class OwnerNameUpdatedEvent extends EventBase<OwnerNameUpdatedEvent> {
    public String ownerId;
    public String name;

    @Incoming("owner-name-updated-incoming")
    public void notify(String e){
        this.notifySubscribers(e, this.getClass());
    }

    public static OwnerNameUpdatedEvent with(ObjectId ownerId, String name) {
        OwnerNameUpdatedEvent e = new OwnerNameUpdatedEvent();
        e.channelName = "owner-name-updated";
        e.ownerId = getObjectIdAsString(ownerId);
        e.name = name;
        return e;
    }

    @Outgoing("owner-name-updated")
    public Flowable<String> createStreamEmitter() {
        return this.createStream("owner-name-updated");
   }
}