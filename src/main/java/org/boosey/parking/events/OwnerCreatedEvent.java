package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import javax.enterprise.context.ApplicationScoped;
import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

@Slf4j
@ApplicationScoped
public class OwnerCreatedEvent extends EventBase<OwnerCreatedEvent> {
    public String ownerId;
    public String spaceId;

    @Incoming("owner-created-incoming")
    public void notify(String e){
        log.info("OwnerCREATED notify");
        this.notifySubscribers(e, this.getClass());
    }

    public static OwnerCreatedEvent with(ObjectId ownerId, String spaceId) {
        OwnerCreatedEvent e = new OwnerCreatedEvent();
        e.channelName = "owner-created";
        e.ownerId = getObjectIdAsString(ownerId);
        e.spaceId = spaceId;
        return e;
    }

    @Outgoing("owner-created")
    public Flowable<String> createStreamEmitter() {
        log.info("Creating owner-created");
        return this.createStream("owner-created");
    }    
}