package org.boosey.parking.events;

import java.util.HashMap;
import java.util.UUID;
import java.util.function.Consumer;
import javax.enterprise.context.ApplicationScoped;
import com.google.gson.Gson;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Emitter;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;

@Slf4j
@ApplicationScoped
public class EventBase<T> {

    public String channelName;
    public static HashMap<String, Emitter<String>> emitters = new HashMap<>();
    protected static Gson gson = new Gson();

    public EventBase() {}

    public UUID subscribe(Consumer<T> fn, Class<T> clazz) {
        log.info("Subscribing to EventBaseG incoming: {}", clazz);
        UUID subscriptionUuid = UUID.randomUUID();
        EventSubscriptions.subscribe(subscriptionUuid, fn, clazz);
        return subscriptionUuid;
    }

    public void unsubscribe(UUID uuid, Class<T> clazz) {
        log.info("Unsubscribing to EventBaseG incoming {}", uuid);
        EventSubscriptions.unsubscribe(uuid, clazz);
    }

    @SuppressWarnings("unchecked")
    public <V> void notifySubscribers(String eString, Class<V> clazz) {
        log.info("notifySubscribers class: {} event string: {}", clazz, eString);
        EventSubscriptions.getSubscriptionsFor(clazz).forEach(
            (uuid, fn) -> {
                log.info("Notifying uuid: {} class: {}", uuid, clazz);
                V e = (V)gson.fromJson(eString, clazz);
                log.info("Notifying event: {} lambda: {}", e, fn);
                ((Consumer<V>) fn).accept(e);
            }
        );
    }

    public void emit() {
        this.emitIf(true);
    }

    public void emitIf(Boolean doEmit) {
        if(doEmit) {
            this.getEmitter().onNext(gson.toJson(this));
        }
    }

    public static void setEmitter(String channelName, Emitter<String> emitter) {
        EventBase.emitters.put(channelName, emitter);
    }

    public Emitter<String> getEmitter() throws RuntimeException {
        Emitter<String> em = emitters.get(this.getChannelName());
        if(em != null)
            return em;
        else
            throw new RuntimeException("EMITTER NOT INIALIZED");
    }

    public String getChannelName() {
        return channelName;
    }

    public static String getObjectIdAsString(ObjectId id) {
        return id.toHexString();
    }

    public Flowable<String> createStream(String channelName) {
        Flowable<String> flowable;
        flowable = Flowable.create(new FlowableOnSubscribe<String>() {
            @Override
            public void subscribe(FlowableEmitter<String> emitter) throws Exception {
                EventBase.setEmitter(channelName, emitter);
            }           
        }, BackpressureStrategy.BUFFER);  
        return flowable;
    }   
}