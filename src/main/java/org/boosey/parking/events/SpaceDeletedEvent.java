package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@ApplicationScoped
public class SpaceDeletedEvent extends EventBase<SpaceDeletedEvent> {
    public String spaceId;

    @Incoming("space-deleted-incoming")
    public void notify(String e){
        this.notifySubscribers(e, this.getClass());
    }

    public static SpaceDeletedEvent with(ObjectId spaceId) {
        SpaceDeletedEvent  e = new SpaceDeletedEvent();
        e.channelName = "space-deleted";
        e.spaceId = getObjectIdAsString(spaceId);
        return e;
    }

    @Outgoing("space-deleted")
    public Flowable<String> createStreamEmitter() {
        return this.createStream("space-deleted");
   }
}