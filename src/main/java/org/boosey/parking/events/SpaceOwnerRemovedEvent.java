package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@Slf4j
@ApplicationScoped
public class SpaceOwnerRemovedEvent extends EventBase<SpaceOwnerRemovedEvent> {
    public String spaceId;

    @Incoming("space-owner-removed-incoming")
    public void notify(String e){
        log.info("OwnerDELETED notify");
        this.notifySubscribers(e, this.getClass());
    }

    public static SpaceOwnerRemovedEvent with(ObjectId spaceId) {
        SpaceOwnerRemovedEvent e = new SpaceOwnerRemovedEvent();
        e.channelName = "space-owner-removed";
        e.spaceId = getObjectIdAsString(spaceId);

        return e;
    }

    @Outgoing("space-owner-removed")
    public Flowable<String> createStreamEmitter() {
        log.info("Creating space-owner-removed");
        return this.createStream("space-owner-removed");
   }
}
