package org.boosey.parking.events;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import io.reactivex.Flowable;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@ApplicationScoped
public class SpaceOwnerSetEvent extends EventBase<SpaceOwnerSetEvent> {
    public String spaceId;
    public String ownerId;

    @Incoming("space-owner-set-incoming")
    public void notify(String e){
        this.notifySubscribers(e, this.getClass());
    }

    public static SpaceOwnerSetEvent with(ObjectId spaceId, String ownerId) {
        SpaceOwnerSetEvent e = new SpaceOwnerSetEvent();
        e.channelName = "space-owner-set";
        e.spaceId = getObjectIdAsString(spaceId);
        e.ownerId = ownerId;
        return e;
    }

    @Outgoing("space-owner-set")
    public Flowable<String> createStreamEmitter() {
        return this.createStream("space-owner-set");
   }
}
